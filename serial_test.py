import serial
import time
import json

ser = serial.Serial( "/dev/ttyUSB0", 115200 )  # open first serial port

# Pretty Printing JSON string back
#print(json.dumps(person_dict, indent = 4, sort_keys=True))

def send_command_to_bridge( command, ser ):
	print("Sending command: {}".format(command))
	ser.write(command.encode())      # Request the details of the Nodes


def get_response_from_bridge( ser ):
	echo = b''
	while (ser.in_waiting > 0 ):
		echo += ser.read()

	if ( len(echo) > 0 ):
		print("Received: {}".format(echo.decode("utf-8")))
	else:
		echo += "{}".encode()
	return echo

if ser.isOpen():

	commands = ['{"command":0"}\n',
				'{"command":1,"destination":2824897929,"message":{"command":20}}\n',
				'{"command":2,"message":{"command":12},"color":63519}}\n',
				'{"command":2,"message":{"command":12},"color":0}}\n']
	current_command = 0

	try:
		# Request the list of connected Nodes:
		send_command_to_bridge('{"command":0"}\n', ser)
		time.sleep(0.5)

		raw_node_list = json.loads(get_response_from_bridge( ser ))

		node_count = raw_node_list["node count"]
		#84763553
		for node_number in range(node_count):
			node_address = raw_node_list["node list"][node_number]
			print("Quering node #{} at {}:".format( node_number, node_address ) )
			command = '{"command":1,"destination":' + str(node_address) + ',"message":{"command":20}}\n'
			send_command_to_bridge(command, ser)
			time.sleep(0.5)

			node_response = json.loads(get_response_from_bridge(ser))
			if "message" in node_response:
				if ( node_response["message"]["Type"] == 0 ):
					time.sleep(1)
					command = '{"command":1,"destination":' + str(node_address) + ',"message":{"command":3}}\n'
					send_command_to_bridge(command, ser)
					get_response_from_bridge(ser)
					time.sleep(1)
					command = '{"command":1,"destination":' + str(node_address) + ',"message":{"command":6}}\n'
					send_command_to_bridge(command, ser)
					get_response_from_bridge(ser)

			time.sleep(5)

	finally:
		ser.close()             # close port